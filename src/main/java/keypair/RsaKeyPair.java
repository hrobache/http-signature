package keypair;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.security.KeyPair;
import java.security.Security;

import org.bouncycastle.jce.provider.BouncyCastleProvider;
import org.bouncycastle.openssl.PEMDecryptorProvider;
import org.bouncycastle.openssl.PEMEncryptedKeyPair;
import org.bouncycastle.openssl.PEMException;
import org.bouncycastle.openssl.PEMKeyPair;
import org.bouncycastle.openssl.PEMParser;
import org.bouncycastle.openssl.jcajce.JcaPEMKeyConverter;
import org.bouncycastle.openssl.jcajce.JcePEMDecryptorProviderBuilder;

public class RsaKeyPair {

	public RsaKeyPair(String keyPath) {
		super();
		Security.addProvider(new BouncyCastleProvider());
		// listProviders();
		try {
			setKeyPair(loadKey(keyPath));
			// setKeyPair();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	private KeyPair keyPair;

	public final KeyPair loadKey(String keyPath) throws PEMException, IOException {
		File privateKeyFile = new File(keyPath);
		PEMParser pemParser = new PEMParser(new FileReader(privateKeyFile));
		PEMDecryptorProvider decProv = new JcePEMDecryptorProviderBuilder().build(null);
		JcaPEMKeyConverter converter = new JcaPEMKeyConverter().setProvider("BC");
	
		Object object = pemParser.readObject();
		KeyPair kp;
	
		if (object instanceof PEMEncryptedKeyPair) {
//			 System.out.println("Encrypted key - we will use provided password");
			kp = converter.getKeyPair(((PEMEncryptedKeyPair) object).decryptKeyPair(decProv));
		} else {
//			 System.out.println("Unencrypted key - no password needed");
			kp = converter.getKeyPair((PEMKeyPair) object);
//			 System.out.println("Public Key:\n\t"+Base64.toBase64String(kp.getPublic().getEncoded()));
			// kp=null;
		}
		pemParser.close();
		return kp;
	}

	public KeyPair getKeyPair() {
		return keyPair;
	}

	public void setKeyPair(KeyPair keyPair) {
		this.keyPair = keyPair;
	}

}
