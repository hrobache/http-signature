package httpSignature;

import java.security.InvalidKeyException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.SecureRandom;
import java.security.Security;
import java.security.Signature;
import java.security.SignatureException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.bouncycastle.jce.provider.BouncyCastleProvider;
import org.bouncycastle.util.encoders.Base64;

public class BCSignatureKey extends AbstractSignatureKey {

	private static class FixedRand extends SecureRandom {
		/**
		 * 
		 */
		private static final long serialVersionUID = 5252081574428551843L;
		MessageDigest sha;
		byte[] state;

		FixedRand() {
			try {
				this.sha = MessageDigest.getInstance("SHA-1", "BC");
				this.state = sha.digest();
			} catch (Exception e) {
				throw new RuntimeException("can't find SHA-1!");
			}
		}

		public void nextBytes(byte[] bytes) {
			int off = 0;

			sha.update(state);

			while (off < bytes.length) {
				state = sha.digest();

				if (bytes.length - off > state.length) {
					System.arraycopy(state, 0, bytes, off, state.length);
				} else {

					System.arraycopy(state, 0, bytes, off, bytes.length - off);
				}

				off += state.length;

				sha.update(state);
			}
		}
	}

	/**
	 * Return a SecureRandom which produces the same value. <b>This is for testing
	 * only!</b>
	 * 
	 * @return a fixed random
	 */
	private static SecureRandom createFixedRandom() {
		return new FixedRand();
	}

	private Signature signature;

	@Override
	protected void getSignature(String signingString) throws SignatureException {
		System.out.println(signingString);
		signature.update(signingString.getBytes());
		byte[] sigBytes = signature.sign();
		System.out.println(Base64.toBase64String(sigBytes));
	}

	public String sign(final String method, final String uri, final Map<String, String> headers, boolean addRequest) {
		List<String> requiredAsList = new ArrayList<String>();
		for (String key : headers.keySet()) {
			requiredAsList.add(key);
		}
		if (addRequest) {
			requiredAsList.add("(request-target)");
		}
		try {
			String[] array=new String[1];
			return sign(method, uri, headers, requiredAsList.toArray(array));
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	public String sign(final String method, final String uri, final Map<String, String> headers,
			String... required) throws Exception {
		signature = Signature.getInstance("SHA256withRSA", "BC");
		signature.initSign(getKeyPair().getPrivate(), createFixedRandom());
		String signingString = getSignatureString(method, uri, headers, required);
		signature.update(signingString.getBytes());
		byte[] sign = signature.sign();
		setBase64EncodedSignature(new String(Base64.encode(sign)));
		String signed = "keyId=\"" + getKeyId() + "\",algorithm=\"rsa-sha256\",headers=\""
				+ join(" ", required).toLowerCase() + "\",signature=\"" + getBase64EncodedSignature() + "\"";
//		System.out.println("Signature computing:");
//		System.out.println(signingString);
//		System.out.println(getBase64EncodedSignature());
		return signed;
	}

	public boolean verify(String base64EncodedSignature, String signingString) throws NoSuchAlgorithmException, NoSuchProviderException, InvalidKeyException, SignatureException {
		signature = Signature.getInstance("SHA256withRSA", "BC");
		signature.initVerify(getKeyPair().getPublic());
//		System.out.println("Signature verifying:");
//		System.out.println(signingString);
//		System.out.println(base64EncodedSignature);
		signature.update(signingString.getBytes());
		boolean verify = signature.verify(Base64.decode(base64EncodedSignature.getBytes()));
		return verify;
	}
	
	public BCSignatureKey(String keyId, String keyPath) {
		super(keyId,keyPath);
		Security.addProvider(new BouncyCastleProvider());
	}

}
