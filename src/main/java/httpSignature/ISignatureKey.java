package httpSignature;

import java.io.IOException;
import java.security.NoSuchAlgorithmException;
import java.security.SignatureException;
import java.util.Map;

public interface ISignatureKey {

	String getSignatureString(String method, String uri, Map<String, String> headers, String... required)
			throws IOException, Exception;

	String getBase64EncodedSignature() throws SignatureException;

	String sign(final String method, final String uri, final Map<String, String> headers, String... required)
			throws Exception;

	String getContentDigestHeader(String content) throws NoSuchAlgorithmException;

}
