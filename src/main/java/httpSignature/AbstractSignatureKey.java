package httpSignature;

import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.SignatureException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.bouncycastle.util.encoders.Base64;

import keypair.RsaKeyPair;

public abstract class AbstractSignatureKey extends RsaKeyPair implements ISignatureKey {

	public static final String NEW_LINE = "\n";
	private String keyId;
	private String base64EncodedSignature;

	public AbstractSignatureKey(String keyId, String keyPath) {
		super(keyPath);
		this.keyId = keyId;
	}

	private static String getValue(String lKey, Map<String, String> headers, String method, String uri)
			throws Exception {
		if ("(request-target)".equals(lKey)) {
			String requestTarget = join(" ", "(request-target):", method.toLowerCase(), uri);
			return requestTarget;
		}
		if ("(created)".equals(lKey)) {
			String requestTarget = join(" ", "(created):", "1402170695");
			return requestTarget;
		}
		if ("(expires)".equals(lKey)) {
			String requestTarget = join(" ", "(expires):", "1402170699");
			return requestTarget;
		}
		final String value = headers.get(lKey);
		if (value == null)
			throw new Exception("Missing Required Header: " + lKey);
		String header = lKey + ": " + value;
		return header;
	}

	private static Map<String, String> toLowercase(final Map<String, String> headers) {
		final Map<String, String> map = new HashMap<String, String>();
		for (final Map.Entry<String, String> entry : headers.entrySet()) {
			map.put(entry.getKey().toLowerCase(), entry.getValue());
		}
		return map;
	}

	public static String join(final String delimiter, final Collection<String> collection) {
		if (collection.size() == 0)
			return "";
		final StringBuilder sb = new StringBuilder();
		for (final Object obj : collection) {
			sb.append(obj).append(delimiter);
		}
		return sb.substring(0, sb.length() - delimiter.length());
	}

	public static String join(final String delimiter, final String... collection) {
		if (collection.length == 0)
			return "";
		final StringBuilder sb = new StringBuilder();
		for (final Object obj : collection) {
			sb.append(obj).append(delimiter);
		}
		return sb.substring(0, sb.length() - delimiter.length());
	}

	protected abstract void getSignature(String signingString) throws SignatureException;

	public abstract String sign(final String method, final String uri, final Map<String, String> headers,
			String... required) throws Exception;

	public String getKeyId() {
		return keyId;
	}

	public String getSignatureString(String method, String uri, Map<String, String> headers, String... required)
			throws Exception {
		method = method.toLowerCase();
		headers = toLowercase(headers);
		final List<String> list = new ArrayList<String>(required.length);
		for (final String key : required) {
			String lKey = key.toLowerCase();
			String value = getValue(lKey, headers, method, uri);
			list.add(value);
		}
		String join = join(NEW_LINE, list);
		return join;
	}

	public void setKeyId(String keyId) {
		this.keyId = keyId;
	}

	public String getBase64EncodedSignature() throws SignatureException {
		return base64EncodedSignature;
	}

	public void setBase64EncodedSignature(String base64EncodedSignature) {
		this.base64EncodedSignature = base64EncodedSignature;
	}

	public String getContentDigestHeader(String content) throws NoSuchAlgorithmException {
		MessageDigest md = MessageDigest.getInstance("SHA-256");
		byte[] hashInBytes = md.digest(content.getBytes(StandardCharsets.UTF_8));
		return "SHA-256=".concat(new String(Base64.encode(hashInBytes)));
	}

	public String getSha256AsBase64(String toDigest) {
		try {
			return getSha256AsBase64(toDigest.getBytes());
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
		} catch (NoSuchProviderException e) {
			e.printStackTrace();
		}
		return null;
	}

	private String getSha256AsBase64(byte[] toDigest) throws NoSuchAlgorithmException, NoSuchProviderException {
		MessageDigest hash = MessageDigest.getInstance("SHA256", "BC");
		hash.update(toDigest);
		String digestAsBase64 = Base64.toBase64String(hash.digest());
		return "SHA-256=" + digestAsBase64;
	}

}
