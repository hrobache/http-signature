package httpSignature;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.io.IOException;
import java.net.URL;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.SignatureException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import org.junit.Test;

import httpSignature.ISignatureKey;

public abstract class AbstractSignatureKeyTest {

	private static final String SIGNATURE_STRING_1 = "date: Sun, 05 Jan 2014 21:31:40 GMT";

	private static final String SIGNATURE_STRING_2 = "(request-target): post /foo?param=value&pet=dog\n" + "host: example.com\n"
			+ SIGNATURE_STRING_1;

	protected static final String ENCODED_SIGNATURE_1 = "SjWJWbWN7i0wzBvtPl8rbASWz5xQW6mcJmn+ibttBqtifLN7Sazz"
			+ "6m79cNfwwb8DMJ5cou1s7uEGKKCs+FLEEaDV5lp7q25WqS+lavg7T8hc0GppauB"
			+ "6hbgEKTwblDHYGEtbGmtdHgVCk9SuS13F0hZ8FD0k/5OxEPXe5WozsbM=";

	protected static final String ENCODED_SIGNATURE_2 = "qdx+H7PHHDZgy4y/Ahn9Tny9V3GP6YgBPyUXMmoxWtLbHpUnXS"
			+ "2mg2+SbrQDMCJypxBLSPQR2aAjn7ndmw2iicw3HMbe8VfEdKFYRqzic+efkb3"
			+ "nndiv/x1xSHDJWeSWkx3ButlYSuBskLu6kd9Fswtemr3lgdDEmn04swr2Os0=";

	protected static final String ENCODED_SIGNATURE_3 = "vSdrb+dS3EceC9bcwHSo4MlyKS59iFIrhgYkz8+oVLEEzmYZZvRs"
			+ "8rgOp+63LEM3v+MFHB32NfpB2bEKBIvB1q52LaEUHFv120V01IL+TAD48XaERZF"
			+ "ukWgHoBTLMhYS2Gb51gWxpeIq8knRmPnYePbF5MOkR0Zkly4zKH7s1dE=";

	private static final String REQUEST_TARGET = "(request-target)";

	private static final String CONTENT_LENGTH = "Content-Length";

	private static final String DIGEST = "Digest";

	protected static final String DATE = "Date";

	private static final String HOST = "Host";

	private static final String CONTENT_TYPE = "Content-Type";

	protected static final String DIGEST_HEADER = "digest: SHA-256=X48E9qOokqqrvdts8nOJRJN3OWDUoyWxBf7kbu9DBPE=";

	private static final String SIGNATURE_STRING_3 = "(request-target): post /foo?param=value&pet=dog\n" + "host: example.com\n"
			+ "date: Sun, 05 Jan 2014 21:31:40 GMT\n" + "content-type: application/json\n" + DIGEST_HEADER
			+ "\n" + "content-length: 18";

	protected static final String KEY_PATH = "httpSignature/PrivateKey.pem";

	protected static final String CONTENT = "{\"hello\": \"world\"}";

	protected static final String METHOD = "POST";

	protected static final String URI = "/foo?param=value&pet=dog";

	protected static final String[] REQUIRED_1 = { DATE };

	protected static final String[] REQUIRED_2 = { REQUEST_TARGET, HOST, DATE };

	protected static final String[] REQUIRED_3 = { REQUEST_TARGET, HOST, DATE, CONTENT_TYPE, DIGEST, CONTENT_LENGTH };

	public static Map<String, String> getRequestHeaders() {
		final Map<String, String> headers = new HashMap<String, String>();
		headers.put(HOST, "example.com");
		headers.put(DATE, "Sun, 05 Jan 2014 21:31:40 GMT");
		headers.put(CONTENT_TYPE, "application/json");
		return headers;
	}

	private ISignatureKey key;

	protected Map<String, String> completeHeaders(Map<String, String> headers) {
		try {
			headers.put(DIGEST, getKey().getContentDigestHeader(CONTENT));
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
		}
		headers.put(CONTENT_LENGTH, String.valueOf(CONTENT.length()));
		return headers;
	}

	protected Map<String, String> getCompleteRequestHeaders() {
		return completeHeaders(getRequestHeaders());
	}

	public ISignatureKey getKey() {
		return key;
	}

	protected String getKeyPath() {
		ClassLoader classLoader = this.getClass().getClassLoader();
		URL resource = classLoader.getResource(KEY_PATH);
		String file = resource.getFile();
		System.out.println("Loading key file "+file);
		return file;
	}

	protected abstract boolean getVerify(String base64EncodedSignature, Map<String, String> headers, String[] required,
			boolean toCorrupt) throws IOException, Exception, NoSuchAlgorithmException, NoSuchProviderException,
			InvalidKeyException, SignatureException;

	private void info(Map<String, String> completeRequestHeaders, String... required) {
		System.out.println("\t"+METHOD+" "+URI);
		listHeaders(completeRequestHeaders);
		System.out.println("\tList of relevant headers:");
		for (String header : required) {
			System.out.println("\t\t"+header);			
		}
	}

	protected void listHeaders(Map<String, String> completeRequestHeaders) {
		System.out.println("\tList of all headers:");
		for(String header : completeRequestHeaders.keySet()) {
			System.out.println("\t\t"+header+": "+completeRequestHeaders.get(header));			
		}
	}

	public void setKey(ISignatureKey key) {
		this.key = key;
	}

	protected void test_Base64EncodedSignature(String expectedBaseEncodedSignature, String expectedSignatureString, String... required) throws Exception {
		Map<String, String> completeRequestHeaders = getCompleteRequestHeaders();
		System.out.println("Computing of signature:");
		info(completeRequestHeaders, required);
		getKey().sign(METHOD, URI, completeRequestHeaders, required);
		String base64EncodedSignature = getKey().getBase64EncodedSignature();
		String signatureString = getKey().getSignatureString(METHOD, URI, completeRequestHeaders, required);
		System.out.println("\tComputed signature string:\n"+signatureString);
		assertEquals(expectedSignatureString, signatureString);
		System.out.println("\tComputed signature: "+base64EncodedSignature);
		assertEquals(expectedBaseEncodedSignature, base64EncodedSignature);
		boolean verify = getVerify(base64EncodedSignature, completeRequestHeaders, required, false);
		assertTrue(verify);
		System.out.println("\tSignature successfully verified!");
		verify = getVerify(base64EncodedSignature, completeRequestHeaders, required,true);
		assertFalse(verify);
		System.out.println("\tSignature is now wrong!\n");
	}

	@Test
	public void test_base64EncodedSignature_1() {
		try {
			test_Base64EncodedSignature(ENCODED_SIGNATURE_1, SIGNATURE_STRING_1, REQUIRED_1);
		} catch (Exception e) {
			e.printStackTrace();
			fail();
		}
	}

	@Test
	public void test_base64EncodedSignature_2() {
		try {
			test_Base64EncodedSignature(ENCODED_SIGNATURE_2, SIGNATURE_STRING_2, REQUIRED_2);
		} catch (Exception e) {
			e.printStackTrace();
			fail();
		}
	}

	@Test
	public void test_base64EncodedSignature_3() {
		try {
			test_Base64EncodedSignature(ENCODED_SIGNATURE_3, SIGNATURE_STRING_3, REQUIRED_3);
		} catch (Exception e) {
			e.printStackTrace();
			fail();
		}
	}

	@Test
	public void test_digestHeader() throws NoSuchAlgorithmException {
		assertEquals(DIGEST_HEADER, "digest: " + getKey().getContentDigestHeader(CONTENT));
	}

	protected void test_SignatureString(String expectedSignatureString, Map<String, String> headers, String... required) throws Exception {
		Map<String, String> completeRequestHeaders = getCompleteRequestHeaders();
		System.out.println("Computing of signature string:");
		info(completeRequestHeaders, required);
		String signatureString = getKey().getSignatureString(METHOD, URI, completeRequestHeaders, required);
		System.out.println("\tComputed signature string:\n"+signatureString+"\n");
		assertEquals(expectedSignatureString, signatureString);
	}

}
