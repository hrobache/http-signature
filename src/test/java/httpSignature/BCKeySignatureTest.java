package httpSignature;

import java.io.IOException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.SignatureException;
import java.util.Map;

import org.junit.Before;

public class BCKeySignatureTest extends AbstractSignatureKeyTest {

	@Before
	public void setUp() throws Exception {
		setKey(new BCSignatureKey("Test", getKeyPath()));
	}

	@Override
	protected boolean getVerify(String base64EncodedSignature, Map<String, String> headers, String[] required, boolean toCorrupt) throws IOException, Exception,
	NoSuchAlgorithmException, NoSuchProviderException, InvalidKeyException, SignatureException {
		if (toCorrupt) {
			System.out.println("\tIntended date value corruption");
			headers.put(DATE, "Now!");
			listHeaders(headers);
		}
		String signingString = getKey().getSignatureString(METHOD, URI, headers, required);
		boolean verify = ((BCSignatureKey) getKey()).verify(base64EncodedSignature, signingString);
		return verify;
	}

}
